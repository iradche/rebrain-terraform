variable "terraform_token" {
    type = "string"
}

variable "ssh_ira" {}
variable "ssh_fingerprint" {}

# Configure the GitLab Provider
provider "gitlab" {
    token    = "${var.terraform_token}"
    base_url = "https://gitlab.rebrainme.com/api/v4/"
}

# Add a project owned by the user
resource "gitlab_project" "rebrain_terraform" {
    name           = "rebrain-terraform"
    default_branch = "master"
}

# Add a deploy key to the project
resource "gitlab_deploy_key" "ira_deploy_key" {
    project = "${gitlab_project.rebrain_terraform.id}"
    title   = "terraform example"
    key     = "${var.ssh_ira}"
}

variable "do_token" {}

# Configure the DigitalOcean Provider
provider "digitalocean" {
  token = "${var.do_token}"
}

resource "digitalocean_ssh_key" "irina" {
  name       = "irina"
  public_key = "${file("/home/ira/.ssh/id_rsa.pub")}"
}

# Create a web server
resource "digitalocean_droplet" "irina" {
    image    = "ubuntu-16-04-x64"
    name     = "irina"
    size     = "s-1vcpu-1gb"
    region   = "nyc1"
    ssh_keys = [
     "${var.ssh_fingerprint}", "${digitalocean_ssh_key.irina.fingerprint}"    
    ]
    tags = [
    "Irina", "Radchenko", "iradche"
    ]
}
