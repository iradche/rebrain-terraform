# Установка Vscale плагина для Terraform

Документация на установку `Vscale` плагина для Terraform.

## Начало работы

Здесь даны краткие инструкции для установки `Vscale` плагина для Terraform.


### Необходимые условия
Необходимо установить следующее программное обеспечение:
-   [Terraform](https://www.terraform.io/downloads.html)  0.10.x
-   [Go](https://golang.org/doc/install)  1.8 (для плагина)
Если у вас не установлен компилятор языка Go (в Ubuntu Linux для проверки того, установлен ли у вас язык Go, можно использовать команду ```$ dpkg -l | grep go```), то поставьте его. 
Для этого выполните команды:
```$ sudo apt-cache search golang``` (уточните полное имя пакета, оно обычно содержит текущую версию языка Go)
```$ sudo apt-get install golang-1.10```

## Установка
Установите переменную окружения, содержащую название провайдера:
```$ export PROVIDER_NAME="vscale"```

Склонируйте репозиторий:  

```
$ mkdir -p $GOPATH/src/github.com/terraform-providers; cd $GOPATH/src/github.com/terraform-providers
$ git clone https://github.com/burkostya/terraform-provider-vscale
```

Войдите в каталог провайдера.
```
$ cd $GOPATH/src/github.com/terraform-providers/terraform-provider-$PROVIDER_NAME
```
Установите средство вендеринга [dep](https://github.com/golang/dep):
```
/usr/lib/go-1.10/bin/go get -u github.com/golang/dep/cmd/dep
```
Установите пакеты:
```~/gopath/bin/dep ensure```
и соберите приложение (путь до компилятора Golang в вашей системе может отличаться):
```/usr/lib/go-1.10/bin/go build```

Скопируйте файл `terraform-provider-vscale` в каталог `~/.terraform.d/plugins`.

После установки плагина, воспользуйтесь командой `terraform init` для инициализации Terraform.
